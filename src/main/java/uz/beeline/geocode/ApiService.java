package uz.beeline.geocode;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.util.*;

@Service
public class ApiService {

    private final OkHttpClient client;
    private final AppConfigs props;

    public ApiService(AppConfigs props) {
        this.props = props;
        if (props.isUseProxy()) {
            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(props.getProxyHost(), props.getProxyPort()));
            this.client = new OkHttpClient.Builder().proxy(proxy).build();
        } else {
            this.client = new OkHttpClient();
        }
    }

    public void getAddresses() {

        int limitOfRequests = getReqLimit();
        if (limitOfRequests < 0) {
            throw new ExceptionHandler("Daily limits are over! Try to re-run applet tomorrow.");
        }

        String path;
        if (props.getFilePath().isEmpty()) {
            path = new File("." + File.separator + props.getFileName()).getPath();
        } else {
            path = new File(props.getFilePath() + File.separator + props.getFileName()).getPath();
        }

        List<String> geocodeList = ExcelService.readData(path, props.getColumnName());

        Map<String, String[]> addressMap = new HashMap<>();
        String[] searchKeyFromResponseList = props.getSearchKeyFromResponse();
        addressMap.put(props.getColumnName(), searchKeyFromResponseList);

        int availableRequestsOfNow = Math.min(limitOfRequests, geocodeList.size());

        System.out.println();
        for (int i = 0; i < availableRequestsOfNow; i++) {
            String geocode = geocodeList.get(i);
            String[] address = getAddress(geocode);
            addressMap.put(geocode, address);
            System.out.println(i+1 + ". " + geocode + ": " + Arrays.toString(address));
        }
        int updatedRows = ExcelService.updateValues(path, props.getColumnName(), addressMap);
        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println("In the file: " + props.getFileName() + " Inserted or Updated <--" + updatedRows + "--> rows." );
        int availableRequestsAfterJob = limitOfRequests - availableRequestsOfNow;
        System.out.println("Available requests are for today after job: " + availableRequestsAfterJob);
        System.out.println("-----------------------------------------------------------------------------------------");
    }

    private int getReqLimit() {
        Request request = makeLimitInfoRequest();
        String responseData = handleResponse(request);
        String geocoderDaily = getValue(new JSONObject(responseData), "apimaps_http_geocoder_daily");
        int totalDayLimit = Integer.parseInt(Objects.requireNonNull(getValue(new JSONObject(geocoderDaily), "limit")));
        int sentRequests = Integer.parseInt(Objects.requireNonNull(getValue(new JSONObject(geocoderDaily), "value")));
        return totalDayLimit - sentRequests;
    }

    private Request makeLimitInfoRequest() {
        String fullUrl = props.getDevBaseUrl() + "projects/" +
                props.getProjectId() +
                "/services/" +
                props.getServiceId() +
                "/limits";
        return new Request.Builder().url(fullUrl)
                .addHeader("Content-Type", "application/json")
                .addHeader("X-Auth-Key", props.getApiKeyForReqInfo())
                .build();
    }

    private String[] getAddress(String geocode) {
        Request request = makeGeoRequest(geocode);
        String responseData = handleResponse(request);
        String[] searchKeyFromResponse = props.getSearchKeyFromResponse();
        String[] results = new String[searchKeyFromResponse.length];
        for (int i = 0; i < searchKeyFromResponse.length; i++) {
            String value = getValue(new JSONObject(responseData), searchKeyFromResponse[i]);
            results[i] = value;
        }
        return results;
//        return getValue(new JSONObject(responseData), props.getSearchKeyFromResponse());
    }

    private String handleResponse(Request request) {
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code: " + response.body().string());
            }
            return response.body().string();
        } catch (IOException e) {
            String exceptionMsg = "OKHTTP Error: ";
            if ((e.getClass().equals(ConnectException.class))) {
                exceptionMsg = "Proxy Connection Error: ";
            } else if ((e.getClass().equals(UnknownHostException.class))) {
                exceptionMsg = "Proxy Unknown Host Error: ";
            }
            throw new ExceptionHandler("Ops! " + exceptionMsg + e.getMessage());
        }
    }

    private Request makeGeoRequest(String geocode) {
        HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(props.getBaseUrl())).newBuilder();
        urlBuilder.addQueryParameter("apikey", props.getApiKey());
        urlBuilder.addQueryParameter("format", props.getResponseFormat());
        urlBuilder.addQueryParameter("sco", props.getSco());
        urlBuilder.addQueryParameter("geocode", geocode);
        urlBuilder.addQueryParameter("results", "1");
        return new Request.Builder().url(urlBuilder.build().toString())
                .addHeader("Content-Type", "application/json")
                .build();
    }

    private String getValue(JSONObject jsonObject, String key) {
        Iterator<String> iterator = jsonObject.keys();
        while (iterator.hasNext()) {
            String innerKey = iterator.next();
            Object innerObj = jsonObject.get(innerKey);
            if (innerKey.equals(key)) {
                return innerObj.toString();
            } else if (innerObj instanceof JSONObject) {
                String result = getValue((JSONObject) innerObj, key);
                if (result != null) {
                    return result;
                }
            } else if (innerObj instanceof JSONArray) {
                JSONArray array = (JSONArray) innerObj;
                for (int i = 0; i < array.length(); i++) {
                    Object arrayObj = array.get(i);
                    if (arrayObj instanceof JSONObject) {
                        String result = getValue((JSONObject) arrayObj, key);
                        if (result != null) {
                            return result;
                        }
                    }
                }
            }
        }
        return null;
    }
}
