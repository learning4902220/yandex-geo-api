package uz.beeline.geocode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YandexGeocodeApiApplication implements CommandLineRunner {

    @Autowired
    ApiService apiService;

    public static void main(String[] args) {
        SpringApplication.run(YandexGeocodeApiApplication.class, args);
    }

    @Override
    public void run(String... args) {
        apiService.getAddresses();
//        System.exit(1);
    }
}
