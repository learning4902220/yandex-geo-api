package uz.beeline.geocode;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Component
@Validated
@ConfigurationProperties(prefix = "configs", ignoreUnknownFields = true)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AppConfigs {

    @NotEmpty
    private String baseUrl;
    @NotEmpty
    private String apiKey;
    @NotEmpty
    private String apiKeyForReqInfo;
    @NotEmpty
    private String projectId;
    @NotEmpty
    private String serviceId;
    @NotEmpty
    private String devBaseUrl;
    private String responseFormat;
    @NotEmpty
    private String sco;
    @NotEmpty
    private String[] searchKeyFromResponse;
    private boolean useProxy;
    @NotEmpty
    private String proxyHost;
    @Min(value = 0L, message = "The ProxyPort must be greater then 0")
    @Max(value = 65536L, message = "The ProxyPort must be less then 65536")
    private Integer proxyPort;

    private String filePath;
    @NotEmpty
    private String fileName;
    @NotEmpty
    private String columnName;
}
