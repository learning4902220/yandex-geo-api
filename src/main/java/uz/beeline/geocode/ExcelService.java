package uz.beeline.geocode;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class ExcelService {

    public static List<String> readData(String path, String columnName) {

        List<String> result = new ArrayList<>();
        File file = new File(path);
        Workbook workbook;
        FileInputStream inputStream;

        try {
            inputStream = new FileInputStream(file);
            if (file.getName().endsWith(".xlsx") || file.getName().endsWith(".xlsm") || file.getName().endsWith(".xlk")) {
                workbook = new XSSFWorkbook(inputStream);
            } else if (file.getName().endsWith(".xls")) {
                workbook = new HSSFWorkbook(inputStream);
            } else {
                throw new ExceptionHandler("Ops! File type not allowed of .xlsx or .xls for " + file.getName());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();

        // Get the column index
        int columnIndex = -1;
        Row headerRow = rowIterator.next();
        Iterator<Cell> cellIterator = headerRow.cellIterator();
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            if (cell.getStringCellValue().equalsIgnoreCase(columnName)) {
                columnIndex = cell.getColumnIndex();
                break;
            }
        }

        if (columnIndex == -1) {
            throw new IllegalArgumentException("Column with name '" + columnName + "' not found in file.");
        }

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Cell cell = row.getCell(columnIndex);
            if (cell == null) {
                continue;
            }

            // Получаем цвет фона ячейки
            short currentColor = cell.getCellStyle().getFillForegroundColor();

            // Проверяем, совпадает ли цвет с ожидаемым
            if (currentColor == 42) {
                // Если цвет совпадает, то пропускаем.
                continue;
            }

            String cellValue = null;
            switch (cell.getCellType()) {
                case STRING:
                    cellValue = cell.getStringCellValue();
                    break;
                case NUMERIC:
                    cellValue = String.valueOf(cell.getNumericCellValue());
                    break;
                case BOOLEAN:
                    cellValue = String.valueOf(cell.getBooleanCellValue());
                    break;
                case FORMULA:
                    cellValue = cell.getCellFormula();
                    break;
                default:
                    break;
            }

            if (cellValue != null && !cellValue.isEmpty()) {
                result.add(cellValue);
            }
        }

        try {
            workbook.close();
            inputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public static int updateValues(String path, String columnName, Map<String, String[]> valueMaps) {
        int updatedRows = -1;
        try {
            FileInputStream fileInputStream = new FileInputStream(path);
            Workbook workbook = WorkbookFactory.create(fileInputStream);
            Sheet sheet = workbook.getSheetAt(0);
            int columnIdx = getColumnIndex(sheet, columnName);
            if (columnIdx == -1) {
                throw new RuntimeException("Column " + columnName + " not found in the sheet");
            }
            for (Row row : sheet) {
                Cell keyCell = row.getCell(columnIdx);
                if (keyCell == null || (!valueMaps.containsKey(keyCell.getStringCellValue()))) {
                    continue;
                }

                // Получаем текущий стиль ячейки
                CellStyle currentStyle = keyCell.getCellStyle();

                CellStyle newStyle = workbook.createCellStyle();
                newStyle.cloneStyleFrom(currentStyle);
                newStyle.setFillForegroundColor((short) 42);
                newStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                keyCell.setCellStyle(newStyle);

                String key = keyCell.getStringCellValue();
                String[] values = valueMaps.get(key);
                for (int i = 0; i < values.length; i++) {
                    Cell cellForInputValue = row.createCell(columnIdx + 1 + i);
                    cellForInputValue.setCellValue(values[i]);
                }
                updatedRows++;
            }
            fileInputStream.close();
            FileOutputStream outFile = new FileOutputStream(path);
            workbook.write(outFile);
            outFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return updatedRows;
    }

    private static int getColumnIndex(Sheet sheet, String columnName) {
        Row firstRow = sheet.getRow(0);
        for (Cell cell : firstRow) {
            if (cell.getStringCellValue().equalsIgnoreCase(columnName)) {
                return cell.getColumnIndex();
            }
        }
        return -1;
    }

    public static void changeCellColorIfMatches(String path, String sheetName, int rowNumber, int columnNumber, short expectedColor, short newColor) throws IOException {
        FileInputStream inputStream = new FileInputStream(new File(path));
        Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(rowNumber);
        Cell cell = row.getCell(columnNumber);
        CellStyle cellStyle = cell.getCellStyle();
        if (cellStyle.getFillForegroundColor() == expectedColor) {
            cellStyle.setFillForegroundColor(newColor);
            cell.setCellStyle(cellStyle);
        }
        FileOutputStream outputStream = new FileOutputStream(new File(path));
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
    }


}
