package uz.beeline.geocode;

class ExceptionHandler extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public ExceptionHandler(String fullErrMsg) {
        super(fullErrMsg);
    }
}
